<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>

  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>


<!--[if IE 6]>
<link type="text/css" rel="stylesheet" media="all" href="<?php print base_path() . path_to_theme() ?>/ie6.css" />
<![endif]-->
</head>

<body>

  <!-- begin container -->
  <div id="container">
	
    <!-- begin header -->
<div class="header-back">
    <div id="header">
		<div class="top-menu">
			<?php print $top_menu; ?>

		</div>
	  <div class="logo_site_slogan">
      <!-- site logo -->
      <?php if ($logo) : ?>
	   <div class="logo">
        <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>">
          <img class="logo" src="<?php print $logo ?>" alt="<?php print t('Home') ?>" />
        </a>
		</div>
      <?php endif; ?>
	  
	  <!-- end site logo -->

      <!-- site name -->
      <?php if ($site_name) : ?>
	  <div class="site-name">
        <h1>
	  <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>">
	    <?php print $site_name; ?>
	  </a>
	  </h1>
	</div>
      <?php endif; ?><!-- end site name -->
	  
      <!-- site slogan -->
      <?php if ($site_slogan) : ?>
        <div class="slogan">
	<?php print $site_slogan; ?>
	  </div>
      <?php endif; ?><!-- end site slogan -->
      </div>
      <div class="header_content">
          <div class="header-image">
            <?php print $header_image ?>
          </div>
          <div class="header_search_text">
              <!-- Serach Box -->
              <div class="top-search">
                  <?php if ($search_box) : ?><label>Search</label><?php print $search_box ?><?php endif ; ?>
              </div>
              <!-- end serach box -->
              <div class="header-text">
                <?php print $header_text ?>
              </div>
          </div>
      </div>
	  <!-- primary links -->
	  <?php if (drupal_is_front_page() == 1) { ?>
			<div id="menu">
			  <?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?>
			</div>
		<?php } else {?>
		<div class="slant-end">
			<div id="menu">
			  <?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?>
			</div>
		</div>
		<?php } ?>
	<!-- end primary links -->
		<?php if ($content_top) : ?>
		<div class="content-top-block">
		<div class="content-top">
			<?php print $content_top; ?>
		</div>
		</div>
		<?php endif; ?>
    </div></div><!-- end header -->

    <!-- content -->
	<div class="main_content_container">
    <!-- begin mainContent -->
    <div id="mainContent" style="width: <?php print Amor_Azul_get_mainContent_width( $sidebar_left, $sidebar_right) ?>px;">
        
    <?php if ($mission): print '<div class="mission">'. $mission .'</div>'; endif; ?>
    <?php if ($breadcrumb): print '<div class="breadcrumb">'. $breadcrumb . '</div>'; endif; ?>
    <?php if ($title) : print '<h1 class="pageTitle">' . $title . '</h1>'; endif; ?>
    <?php if ($tabs) : print '<div class="tabs">' . $tabs . '</div>'; endif; ?>
    <?php if ($help) : print '<div class="help">' . $help . '</div>'; endif; ?>
    <?php if ($messages) : print '<div class="messages">' .$messages . '</div>'; endif; ?>
    <?php print $content; ?>
    <?php print $content_bottom; ?>
    <?php print $feed_icons; ?>

    </div><!-- end mainContent -->

    <!-- begin sideBars -->

    <div id="sideBars-bg" style="width: <?php print Amor_Azul_get_sideBars_width( $sidebar_left, $sidebar_right) ?>px;">
      <div id="sideBars" style="width: <?php print Amor_Azul_get_sideBars_width( $sidebar_left, $sidebar_right) ?>px;">

        <!-- left sidebar -->
        <?php if ($sidebar_left && arg(0)!='admin') : ?>
          <div id="leftSidebar">
		  <div class="sidebar-left-top"></div>
		  	<div class="leftbar-content">
            <?php print $sidebar_left; ?>
			</div>
			<div class="sidebar-left-bottom"></div>
          </div>
        <?php endif; ?>
        
        <!-- right sidebar -->
        <?php if ($sidebar_right) : ?>
          <div id="rightSidebar">
            <?php print $sidebar_right; ?>
          </div>
        <?php endif; ?>

      </div><!-- end sideBars -->
    </div><!-- end sideBars-bg -->
    </div>
  </div><!-- end container -->
  <!-- footer -->
    <div id="footer">
	  <div class="footer-content">
	  	<div class="footer-message">
			<?php print $footer_message; ?><br />
			Powered by Drupal, Design &amp; Developed by <a href="http://www.cmswebsiteservices.com"> CMS Website Services</a>, LLC
		 </div>
		 <div class="footer-link">
		  <?php print $footer_links; ?>
		 </div>
	  </div>
    </div><!-- end footer -->
  
  <?php print $closure ?>
</body>
</html>
