<?php
// Amor_Azul

/**
 * Declare the available regions implemented by this engine.
 *
 * @return
 *    An array of regions.  The first array element will be used as the default region for themes.
 *    Each array element takes the format: variable_name => t('human readable name')
 */
function Amor_Azul_regions() {
    return array(
		'top_menu' => t('top menu'),
        'header_image' => t('header image'),
		'header_text' => t('header text'),
		'content_top' => t('content top'),
        'sidebar_left' => t('sidebar left'),
        'sidebar_right' => t('sidebar right'),
        'content_bottom' => t('content bottom'),
        'footer_links' => t('footer_links')
    );
}

/**
* Adjust content width according to the absence or presence of sidebars.
*
*   If only one sidebar is active, the mainContent width will expand to fill
*   the space of the missing sidebar.
*/
function Amor_Azul_get_mainContent_width($sidebar_left, $sidebar_right) {
  $width = 430;
  if (!$sidebar_left || arg(0)=='admin') {
    $width = $width + 180;
  }  
  if (!$sidebar_right) {
    $width = $width + 180;
  }  
  return $width;
}
function Amor_Azul_get_sideBars_width($sidebar_left, $sidebar_right) {
  $width = 415;
  if (!$sidebar_left || arg(0)=='admin') {
    $width = $width - 205;
  }  
  if (!$sidebar_right) {
    $width = $width - 205;
  }  
  return $width;
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
   if (!empty($breadcrumb)) {
     return '<div class="breadcrumb">'. implode(' :: ', $breadcrumb) .'</div>';
   }
 }

/**
* Catch the theme_links function 
*/
function phptemplate_links($links, $attributes = array('class' => 'links')) {
$output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = '';

      // Automatically add a class to each link and also to each LI
      if (isset($link['attributes']) && isset($link['attributes']['class'])) {
        $link['attributes']['class'] .= ' ' . $key;
        $class = $key;
      }
      else {
        $link['attributes']['class'] = $key;
        $class = $key;
      }

      // Add first and last classes to the list of links to help out themers.
      $extra_class = '';
      if ($i == 1) {
        $extra_class .= 'first ';
      }
      if ($i == $num_links) {
        $extra_class .= 'last ';
      }
      $output .= '<li class="'. $extra_class . $class .'">';

      // Is the title HTML?
      $html = isset($link['html']) && $link['html'];

      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
} 

/**
* Customize a TinyMCE theme.
*
* @param init
*   An array of settings TinyMCE should invoke a theme. You may override any
*   of the TinyMCE settings. Details here:
*
*    http://tinymce.moxiecode.com/wrapper.php?url=tinymce/docs/using.htm
*
* @param textarea_name
*   The name of the textarea TinyMCE wants to enable.
*
* @param theme_name
*   The default tinymce theme name to be enabled for this textarea. The
*   sitewide default is 'simple', but the user may also override this.
*
* @param is_running
*   A boolean flag that identifies id TinyMCE is currently running for this
*   request life cycle. It can be ignored.
*/
function phptemplate_tinymce_theme($init, $textarea_name, $theme_name, $is_running) {

  switch ($textarea_name) {
    // Disable tinymce for these textareas
    case 'log': // book and page log
    case 'img_assist_pages':
    case 'caption': // signature
    case 'pages':
    case 'access_pages': //TinyMCE profile settings.
    case 'user_mail_welcome_body': // user config settings
    case 'user_mail_approval_body': // user config settings
    case 'user_mail_pass_body': // user config settings
    case 'synonyms': // taxonomy terms
    case 'description': // taxonomy terms
      unset($init);
      break;

    // Force the 'simple' theme for some of the smaller textareas.
    case 'signature':
    case 'site_mission':
    case 'site_footer':
    case 'site_offline_message':
    case 'page_help':
    case 'user_registration_help':
    case 'user_picture_guidelines':
      $init['theme'] = 'simple';
      foreach ($init as $k => $v) {
        if (strstr($k, 'theme_advanced_')) unset($init[$k]);
      }
      break;
  }

  // Add some extra features when using the advanced theme. 
  // If $init is available, we can extend it
  if (isset($init)) {
    switch ($theme_name) {
     case 'advanced':
   $init['width'] = '100%';
       break;
  
    }
  }

  // Always return $init
  return $init;
}


function Amor_Azul_search_theme_form($form) {

	//$form['search_theme_form_keys']['#value'] =t('SEARCH');
   //$form['search_theme_form_keys']['#attributes'] = array('onFocus'=>'this.value=""');
	//$form['submit']['#value'] ='';

	$form['submit']['#attributes'] =array('class'=>'SW');
	$form['submit']['#value'] =t('Go');
	//print_r($form);

	//return $form;

	return drupal_render($form);

}



function get_link($menu_path)
{
	if($menu_path=='-')
	{
		$return = '#';
	}
	else
	{
		global $base_url;
		$menu_path = trim($menu_path);
		$db_links = "select * from url_alias where src='".$menu_path."'";
		$rs_links = db_query($db_links);
		if(db_num_rows($rs_links)>0)
		{
			$row_links = db_fetch_object($rs_links);
			$return = $base_url.'/'.$row_links->dst;
		}else
		{
			if($menu_path=='<front>')
			{
				$return = $base_url.'/';
			}elseif(strstr($menu_path,'http://'))
				{
					$return = $menu_path;
			    }
				else
				{
					$return = $base_url.'/'.$menu_path;
				}	
		}
	}
	return $return;
}

function _get_class(&$node,$path)
{
	$return = 'sub';
/*	print 'Node: '.$node;
	print '<br>';
	print 'Node Path: '.$node->path;
	print '<br>';
	print 'Menu Path: '.$path.'<hr>';
*/	
	if($node->path == trim($path) || (($path == '<front>' || $path == 'node/1') && !$node) && arg(0) != 'image')
	{
		$return = 'current';
	}else if(trim('node/'.$node->nid) == trim($path))
	{
		$return = 'current';
	}else if(trim($path) == 'image' && !$node && arg(0) == 'image')
	{
		$return = 'current';
	}
	return $return;
}

function _get_second_leval($parent_id,$menu_type)
{
	$all_menu_1 = '';
	$db_main_menu_1 = "select * from {menu} where pid=".$parent_id." and type IN (".$menu_type.") order by weight";
	$rs_main_menu_1 = db_query($db_main_menu_1);
	if(db_num_rows($rs_main_menu_1)>0)
	{
		$all_menu_1 .= '<ul>';
		while($row_main_menu_1 = db_fetch_object($rs_main_menu_1))
		{
			$all_menu_1 .= '<li>';
			if(strstr($row_main_menu_1->path,'http://'))
			{
				$all_menu_1 .= '<a href="'.get_link($row_main_menu_1->path).'" target="_blank" title="'.$row_main_menu_1->description.'">'.$row_main_menu_1->title.'</a>';
			}
			else
			{
				$all_menu_1 .= '<a href="'.get_link($row_main_menu_1->path).'" title="'.$row_main_menu_1->description.'">'.$row_main_menu_1->title.'</a>';
			}	
			$all_menu_1 .= '</li>';
		}
		$all_menu_1 .= '</ul>';
	}
	return $all_menu_1;
}

function _get_menus($parent_id,$menu_type, $node = '<front>')
{
	$menu_type = explode("|",$menu_type);
	$menu_type = implode(",",$menu_type);
	$all_menu = '';
	$db_main_menu = "select * from {menu} where pid=".$parent_id." and type IN (".$menu_type.") order by weight";
	$rs_main_menu = db_query($db_main_menu);
	if(db_num_rows($rs_main_menu)>0)
	{
		while($row_main_menu = db_fetch_object($rs_main_menu))
		{
			$all_menu .= '<li class="top">';
			$class = _get_class($node,$row_main_menu->path);
			if(strstr($row_main_menu->path,'http://'))
			{
				$all_menu .= '<a href="'.get_link($row_main_menu->path).'" target="_blank" title="'.$row_main_menu->description.'" class="top_link"><span>'.$row_main_menu->title.'</span><!--[if IE 7]><!--></a><!--<![endif]-->';
			}
			else
			{
				$all_menu .= '<a href="'.get_link($row_main_menu->path).'" title="'.$row_main_menu->description.'" class="top_link"><span>'.$row_main_menu->title.'</span><!--[if IE 7]><!--></a><!--<![endif]-->';
			}	
			$all_menu .= '<!--[if lte IE 6]><table><tr><td><![endif]-->';
			$all_menu .= _get_second_leval($row_main_menu->mid,$menu_type);
			$all_menu .= '<!--[if lte IE 6]></td></tr></table></a><![endif]-->';
			$all_menu .= '</li>';
		}
	}
	return $all_menu;
}
if(check_block(3,'Amor_Azul'))
{
db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'block', '3', 'Amor_Azul', '1', '0', 'header_text', '0', '0', '0', '', ''
    );
    db_query("INSERT INTO {boxes} (bid, body, info, format) VALUES (%d, '%s', '%s', '%s')",
        '3',
        '<h1 class="header-title">Leo eu adipiscing eleifend</h1>

              Leo eu adipiscing eleifend, magna urna mattis nibh, sed consequat nisi nibh sit amet nunc. Nam neque. Nam a odio sed tellus mattis posuere. Cras mattis viverra metus. Fusce feugiat elementum metus. Maecenas euismod dignissim diam. Aliquam felis. Nulla augue turpis, euismod id, dictum sed, mollis et, quam. Pellentesque nisl. Cras faucibus ante in sem. Aenean porta diam molestie urna',
        'header text',
        '3'
    );
}	
if(check_block(4,'Amor_Azul'))
{

    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'block', '4', 'Amor_Azul', '1', '0', 'header_image', '0', '0', '0', '', ''
    );
    db_query("INSERT INTO {boxes} (bid, body, info, format) VALUES (%d, '%s', '%s', '%s')",
        '4',
        '<img src="'.base_path().path_to_theme().'/images/header-image.jpg" alt="Girl" />',
        'header images',
        '3'
    );
}
if(check_block(1,'Amor_Azul'))
{
	
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'block', '1', 'Amor_Azul', '1', '0', 'footer_links', '0', '0', '0', '', ''
    );
    db_query("INSERT INTO {boxes} (bid, body, info, format) VALUES (%d, '%s', '%s', '%s')",
        '1',
        '<a href="#">Home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">About Us</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Services</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Contact Us</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
        'Footer Links',
        '3'
    );
}
if(check_block(2,'Amor_Azul'))
{
	
    db_query(
        "INSERT INTO {blocks} (module,delta,theme,status,weight,region,custom,throttle,visibility,pages,title)
        VALUES ('%s', '%s', '%s', %d, %d, '%s', %d, %d, %d, '%s', '%s')",
        'block', '2', 'Amor_Azul', '1', '0', 'content_top', '0', '0', '1', '<front>', ''
    );
    db_query("INSERT INTO {boxes} (bid, body, info, format) VALUES (%d, '%s', '%s', '%s')",
        '2',
        '<div class="cont-top1">

                    <h3>Cum sociis natoque penatibus</h3>

                    <img src="'.base_path().path_to_theme().'/images/home-top-img1.jpg" align="right"  alt="image" />Nunc nisi elit, sollicitudin volutpat, adipiscing eu, rutrum sed, ligula.

                </div>

                <div class="cont-top2">

                    <h3>Cum sociis natoque penatibus</h3>

                    <img src="'.base_path().path_to_theme().'/images/home-top-img2.jpg" align="right" alt="image" />Nunc nisi elit, sollicitudin volutpat, adipiscing eu, rutrum sed, ligula.

                </div>

                <div class="cont-top2">

                    <h3>Cum sociis natoque penatibus</h3>

                    <img src="'.base_path().path_to_theme().'/images/home-top-img3.jpg" align="right" alt="image" />Nunc nisi elit, sollicitudin volutpat, adipiscing eu, rutrum sed, ligula.

                </div>',
        'content top',
        '3'
    );
}	
function check_block($bid,$theme)
{
    $result = db_query("SELECT * FROM {blocks} WHERE delta = %d and theme = '%s' and module = 'block'", $bid, $theme);
	if(db_num_rows($result)>0)
	{
		return false;
	}
	else
	{
		return true;
	}
}